# POLYnomial MANI.PuLation

A symbolic polynomial calculator in Prolog.

## How to run

In a terminal, run

    $ swipl

Inside the REPl, load the file
   
    ?- ["polymani.pl"].

> Note: Don't forget the dot at the end of the line.

## Tests

```
:- ["polymani.pl"].
%@ true.

?- polyplay.
%@ true.

> simplify 1 plus x plus 1 plus x plus 1 plus x plus 1 plus x raised to 3 plus 5 times x raised to 3 plus 42 times x raised to 1337 plus 0
42*x^1337+6*x^3+3*x+4

> show 1 plus x plus 1 plus x plus 1 plus x plus 1 plus x raised to 3 plus 5 times x raised to 3 plus 42 times x raised to 1337 plus 0
1+x+1+x+1+x+1+x^3+5*x^3+42*x^1337+0

> multiply 2 times x squared plus 3 times x plus 5 times x raised to 17 minus 7 times x raised to 21 plus 3 times x raised to 3 minus 23 times x raised to 4 plus 25 times x raised to 5 minus 4.3 by 42
1050*x^5+210*x^17+126*x^3+126*x+84*x^2-294*x^21-966*x^4-180.6

> add 2 times x raised to 2 plus 3 times x plus 5 times x raised to 17 minus x raised to 4 plus 25 times x raised to 5 minus 4.3 to 42 times x raised to 1337 plus 0 minus 5
42*x^1337+25*x^5+5*x^17+3*x+2*x^2-x^4-9.3

> show two plus x squared
2+x^2

> multiply three by two plus x squared
3*x^2+6

> simplify two plus two plus one times y
y+4

> show two plus x squared as P1
P1 = 2+x^2

> multiply three by P1
3*x^2+6

> multiply three by P1 as P2
P2 = 3*x^2+6

> add P1 with x raised to 3
x^3+x^2+2

> show stored polynomials
P1 = 2+x^2
P2 = 3*x^2+6

> forget P1 and show stored polynomials
P2 = 6+3*x^2

> add two times x to four times x
6*x

> show two times x plus three times y raised to four
2*x+3*y^4

> show two x squared
2*x^2

> simplify polynomial five x plus 3 times x
8*x

> bye
See ya

?- poly2list(2*x^2+3*x+5*x^17-7*x^21+3*x^3+25*x^5-4.3, S).
%@ S = [-4.3, 25*x^5, 3*x^3, -7*x^21, 5*x^17, 3*x, 2*x^2].

?- simpoly_list([x*x*x, x^3, 5*x^3, 4.2*z, 2, 42.6, 42*y, 5*z, z^7, z*y^1337, 0], L).
%@ L = [44.6, 7*x^3, 9.2*z, 42*y, y^1337*z, z^7].

?- simpoly(1+x+1+x+1+x+1+x^3+5*x^3+42*x^1337+0, S).
%@ S = 42*x^1337+6*x^3+3*x+4.

?- scalepoly(2*x^2+3*x+5*x^17-7*x^21+3*x^3-23*x^4+25*x^5-4.3, 42, S).
%@ S = 1050*x^5+210*x^17+126*x^3+126*x+84*x^2-294*x^21-966*x^4-180.6.

?- addpoly(2*x^2+3*x+5*x^17-x^4+25*x^5-4.3, 42*x^1337+0-5, S).
%@ S = 42*x^1337+25*x^5+5*x^17+3*x+2*x^2-1*x^4-9.3.
```

## Command Line Interface without NLP

The user available predicates are:
  1) poly2list/2
  2) simpoly_list/2
  3) simpoly/2
  4) scalepoly/3
  5) addpoly/3

`poly2list/2` - transforms a list representing a polynomial (second argument) 
into a polynomial represented as an expression (first argument) 
and vice-versa.

`simpolylist/2` - simplifies a polynomial represented as a list into
another polynomial as a list.

`simpoly/2` - simplifies a polynomial represented as an expression
as another polynomial as an expression.

`scalepoly/3` - multiplies a polynomial represented as an expression by a scalar
resulting in a second polynomial. The two first arguments are assumed to
be ground. The polynomial resulting from the sum is in simplified form.

`addpoly/3` - adds two polynomials as expressions resulting in a
third one. The two first arguments are assumed to be ground.
The polynomial resulting from the sum is in simplified form.

> Note: `foo/N` means the funciton `foo` has `N` parameters (this is known as arity).
  These names are the ones requested in the assignment.

## Authors

* **Diogo Cordeiro** - up*201705417*@fc.up.pt
* **Hugo Sales** - up*201704178*@fc.up.pt

## License

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but
WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public
License along with this program, in the file "COPYING".  If not, see
<http://www.gnu.org/licenses/>.

    IMPORTANT NOTE: The GNU Affero General Public License (AGPL) has
    *different requirements* from the "regular" GPL. In particular, if
    you make modifications to the POLYnomial MANI.PuLation source code on your server,
    you *MUST MAKE AVAILABLE* the modified version of the source code
    to your users under the same license. This is a legal requirement
    of using the software, and if you do not wish to share your
    modifications, *YOU MAY NOT INSTALL POLYNOMIAL MANI.PULATION*.

Additional library software has been made available (and were referenced
in the "Built With" section. All of it is Free Software and can be
distributed under liberal terms, but those terms may differ in detail
from the AGPL's particulars. See each package's license file in their
official repository for additional terms.
